A very simple starter project using:

* PlatformIO
* STM32 Blue Pill Development Board
* Black Magic Probe

It flashes the build in LED of the Blue Pill Development Board and transmits ON/OFF
messages to the UART (Serial Monitor) at 115200 baud.

See also the "Experimenting with a Blue Pill, Black Magic Probe and PlatformIO" blog
post on [cocoacrumbs](http:\\www.cocoacrumbs.com)
